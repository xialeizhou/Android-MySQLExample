package com.livrose.mysqlexample;

import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends Activity {

    TextView requestText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestText = (TextView) findViewById(R.id.requestText);
        Button btnCallPhp = (Button) findViewById(R.id.btnCallPhp);

        btnCallPhp.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        // Alert message
                        Toast.makeText(getBaseContext(), "Contacting server, please wait.", Toast.LENGTH_LONG).show();

                        //Create URLConnection and connection string
                        try {
                            URL url = new URL("http://mackintosh.io/static/files/scripts/test.php");
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            InputStream is = conn.getInputStream();
                            // Manipulate data
                            requestText.setText(is.read());
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }).start();

            }
        });
    }
}






